/*
    Podpunkt B

    Uruchomić powyższy program poprzez funkcję execlp w procesie potomnym innego
    procesu (z użyciem funkcji fork) i wysyłać do niego sygnały poprzez funkcję syste-
    mową kill z procesu macierzystego. ! Uwaga: Przed wysłaniem sygnału sprawdzić,
    czy proces istnieje (patrz podrozdzia l 3.2). Proces macierzysty powinien zaczekać
    na zakończenie swojego potomka przy pomocy funkcji wait, a następnie wypisać
    jego PID i status zakończenia zwracane przez tę funkcję. W przypadku zakończenia
    procesu potomnego przez sygnał powinien wypisać dodatkowo numer tego sygnału
    zawarty w statusie zakończenia, a tak ̇że jego nazwę (użyć funkcji strsignal). Do
    wyłuskania numeru sygnału ze statusu zakończenia użyć makr opisanych w podroz-
    dziale 2.4.
*/

#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 500
#define _GNU_SOURCE

#include <unistd.h>
#include <sys//types.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "helpers.h"

/*
 sygnał wysyłanu do procesu potomnego
 nie musi być identyczny z sygnałem obsługiwanym przez tamten proces
 wtedy status zakończenia procesu będzie zawierać numer sygnału, który go zatrzymał
*/
#define SENT_SIGNAL 5

int main() {
    /* konwersja numeru sygnału na tekst bo wymaga tego execlp */
    char sig_arg[12];
    snprintf(sig_arg, 12, "%d", HANDLED_SIGNAL);

    /* generowanie ścieżki programum który ma zostać uruchomiony */
    char path[100];
    snprintf(path, 100, "%s%s", PATH_PREFIX, A_PROG);

    pid_t pid = fork();
    switch(pid) {
        case -1:
            perror("błąd forkowania: ");
            exit(EXIT_FAILURE);
        case 0:
            /* proces potomny -- uruchamianie programu z podpunku a */
            execlp(path, A_PROG, HANDLE_MODE, sig_arg, (char*)NULL);
            perror("błąd execlp: ");
            exit(EXIT_FAILURE);
        default:
            /* proces macierzysty -- czekamy sekundę i wychodzimy ze switch */
            sleep(1);
    }

    /* sprawdzanie czy proces istnieje */
    if(kill(pid, 0) == -1) {
        printf("proces nie istnieje\n");
        exit(EXIT_FAILURE);
    }
    
    /* wysłanie sygnału (podanego w makrze SENT_SIGNAL) */
    if(kill(pid, SENT_SIGNAL) == -1) {
        perror("błąd wysyłania sygnału: ");
        exit(EXIT_FAILURE);
    }

    /* oczekiwanie na zakończenie procesu potomnego */
    wait_print();

    return 0;
}
ĆWICZENIE 3
-----------

Autor: Daniel Florek


Zawartość
---------
    a.c
        podpunkt A

        program ustawia obsługę sygnału w sposób podany w argumentach
        czeka na pierwszy sygnał po czym przerywa swoje działanie

        ustawienia
            > obsługa trybu uruchamiania
                DEFAULT_MODE - tekst oznaczający domyślną obsługę sygnału
                IGNORE_MODE - tekst oznaczający ignorowanie sygnału
                CUSTOM_MODE - tekst oznaczający własną obsługę sygnału


    b.c
        podpunkt B (wymaga kompilacji z helpers.o)

        uruchamia program z podpunktu A w procesie potomnym
        a nastepnie wysyła do niego sygnał

        ustawienia
            SENT_SIGNAL - numer wysyłanego sygnału do procesu potomnego
            > helpers.h - ustawienia uruchamiania programu z podpunktu A
    
    c.c
        pierwszy program z podpunktu C (wymaga kompilacji z helpers.o)

        tworzy proces potomny i uruchamia w nim program z drugiej części podpunktu

        ustawienia
            PROG - nazwa skompilowanego programu z drugiej częśći podpunktu
            > helpers.h - ustawienia uruchamiania programu z podpunktu A

    c2.C
        drugi program z podpunktu C (wymaga kompilacji z helpers.o)

        tworzy kilka procesów potomnych, w których poprzez execlp uruchamia program z podpunktu A

        ustawienia
            N_PROC - liczba porcesów potomnych do utworzenia
            > helpers.h - ustawienia uruchamiania programu z podpunktu A

    helpers.h
        nagłówek pomocniczy dla programów b.c. c.c i c2.c

        zawiera sygnaturę funcji wait_print, która czeka na proces potomny oraz wypisuje status jego zakończenia
        zawiera także kilka makr pomocniczych i z ustawieniami dla uruchamiania programu z podpunkt

        ustawienia
            HANDLED_SIGNAL - sygnał do obsłużenia
            HANDLE_MODE - tryb obsługi sygnału
            A_PROG - nazwa skompilowanego programu z podpunktu A
            PATH_PREFIX - tekst doklejany do nazwy programu w celu utworzenia ścieżki

    helpers.c
        implementacja wait_print

Uruchamianie
------------
    make runA
    make runB
    make runC
        kompiluje i uruchamia odpowiedni podpunkt

    make all
        kompiluje wszystkie zadania

    make clean
        usuwa pliki wykonywalne

    make tar
        kompresuje folder

#pragma once

#include <fcntl.h>
#include <sys/stat.h>
#include <mqueue.h>

#define KOLEJKA_SERWERA "/so_kojelka_serwera" // nazwa kolejki serwera
#define MAX_MSG 10          // maksymalna liczba komunikatów w kolejce
#define MESSAGE_SIZE 50     // maksymalna długość komunikatu
#define PID_SIZE 10         // maksymalna nazwy kolejki klienta czyli /[pid]

// tworzy kolejkę o podanej nazwie z limitem MAX_MSG komunikatów i rozmiarem wiadomości MESSAGE_SIZE
// zwrócona kolejka jest w trybie tylko do odczytu
mqd_t safe_create_queue(const char *name); 

// otwiera kolejkę o podanej nazwie i trybem otwarcia określonym przez flags
mqd_t safe_open_queue(const char *name, int flags);

// zamyka podaną kolejkę
void safe_close_queue(mqd_t queue);

// usuwa kolejkę o podanej nazwie
void safe_delete_queue(const char *name);

// wysyła wiadomość do kolejki (o długości MESSAGE_SIZE i priority 0)
void safe_send_message(mqd_t queue, const char *msg_ptr);

// odbiera wiadomość z kolejki i zapisuje w msg_ptr (bufor musi mieć długość MESSAGE_SIZE, a priority jest ignorowane)
void safe_receive_message(mqd_t queue, char *msg_ptr);

// zwraca atrybuty kolejki
void safe_get_queue_attributes(mqd_t queue, struct mq_attr *attr);

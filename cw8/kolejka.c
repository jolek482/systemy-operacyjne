#include <stdio.h>
#include <stdlib.h>
#include "kolejka.h"

mqd_t safe_create_queue(const char *name) {
    struct mq_attr attr;
    attr.mq_flags = 0; // tryb zwykły
    attr.mq_maxmsg = MAX_MSG; // maksymalna liczba komunikatów w kolejce
    attr.mq_msgsize = MESSAGE_SIZE; // maksymalny rozmiar komunikatu
    attr.mq_curmsgs = 0; // ilość aktualnych komunikatów - useless

    // tylko do odczytu
    mqd_t queue = mq_open(name, O_CREAT | O_EXCL | O_RDONLY, 0644, &attr);
    if (queue == -1) {
        perror("błąd tworzenia kolejki: ");
        exit(EXIT_FAILURE);
    }

    printf("=== Kolejka [%s] utworzona ===\n", KOLEJKA_SERWERA);
    printf("Flagi: %ld\n", attr.mq_flags);
    printf("Maksymalna liczba komunikatów: %ld\n", attr.mq_maxmsg);
    printf("Maksymalny rozmiar komunikatu: %ld\n", attr.mq_msgsize);
    printf("Liczba komunikatów w kolejce: %ld\n\n", attr.mq_curmsgs);

    return queue;
}

mqd_t safe_open_queue(const char *name, int flags) {
    mqd_t queue = mq_open(name, flags);
    if (queue == -1) {
        perror("błąd otwierania kolejki: ");
        exit(EXIT_FAILURE);
    }
    return queue;
}

void safe_close_queue(mqd_t queue) {
    if (mq_close(queue) == -1) {
        perror("błąd zamykania kolejki: ");
        exit(EXIT_FAILURE);
    }
}

void safe_delete_queue(const char *name) {
    if (mq_unlink(name) == -1) {
        perror("błąd usuwania kolejki: ");
        exit(EXIT_FAILURE);
    }
}

void safe_send_message(mqd_t queue, const char *msg_ptr) {
    if (mq_send(queue, msg_ptr, MESSAGE_SIZE, 0) == -1) {
        perror("błąd wysyłania komunikatu: ");
        exit(EXIT_FAILURE);
    }
}

void safe_receive_message(mqd_t queue, char *msg_ptr) {
    if (mq_receive(queue, msg_ptr, MESSAGE_SIZE, NULL) == -1) {
        perror("błąd odbierania komunikatu: ");
        exit(EXIT_FAILURE);
    }
}

void safe_get_queue_attributes(mqd_t queue, struct mq_attr *attr) {
    if (mq_getattr(queue, attr) == -1) {
        perror("błąd pobierania atrybutów kolejki: ");
        exit(EXIT_FAILURE);
    }
}
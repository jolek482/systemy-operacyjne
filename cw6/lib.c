/*

Autor: Daniel Florek

Biblioteka z funckjami obsługującymi semafory

*/

#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <semaphore.h>
#include <fcntl.h>
#include "lib.h"

void exit_all() {
    /*
    * zamiast exit(EXIT_FAILURE)
    * uruchamiam sygnał SIGINT dla wszystkich naszych procesów
    * co pozwala zatrzymać wszystkie procesy potomne
    * oraz prawidłowo usunąć semafor
    */

    pid_t group_id = getpgrp();
    if(group_id == -1) {
        perror("błąd identyfikatora grupy: ");
    } else {
        kill(-group_id, SIGUSR1);
    }
}

sem_t* safe_sem_create(char * name) {
    sem_t* semaphore = sem_open(name, O_CREAT | O_EXCL, 0644, 1);
    if(semaphore == SEM_FAILED) {
        perror("błąd tworzenia semafora: ");
        exit_all();
    }
    printf("[%p] semaphore created with value %d\n", (void*)semaphore, safe_sem_getvalue(semaphore));

    return semaphore;
}

sem_t* safe_sem_open(char* name) {
    sem_t* semaphore = sem_open(name, 0);
    if(semaphore == SEM_FAILED) {
        perror("błąd otwierania semafora: ");
        exit_all();
    }
    return semaphore;
}

int safe_sem_getvalue(sem_t* semaphore) {
    int semaphore_value;
    if(sem_getvalue(semaphore, &semaphore_value) == -1) {
        perror("błąd pobierania wartości semafora: ");
    }
    return semaphore_value;
}

void safe_sem_wait(sem_t* semaphore) {
    if(sem_wait(semaphore) == -1) {
        perror("\tbłąd oczekiwania na semafor: ");
        exit_all();
    }
}

void safe_sem_release(sem_t* semaphore) {
    if(sem_post(semaphore) == -1) {
        perror("\tbłąd zwalniania semafora: ");
        exit_all();
    }
}

void safe_sem_close(sem_t* semaphore) {
    if(sem_close(semaphore) == -1) {
        perror("błąd zamykania semafora: ");
    }
}

void safe_sem_delete(char* name) {
    if(sem_unlink(name) == -1) {
        perror("błąd usuwania semafora: ");
    }
}


/*

Autor: Daniel Florek

Przykładowy program z sekcją krytyczną

Program odczytuje liczbę z pliku i zwiększa ją o 1 kilka razy
Wykorzystywana jest synchronizacja semaforami

*/

#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include "lib.h"

#define BUFFER_SIZE 10

sem_t* semaphore = NULL;
int file = -1;

void signal_handler(int signum);
void exit_handler();

int main(int argc, char* argv[]) {
    /*
    * Odczytuje liczbę i dodaje do niej 1 odpowiednią ilość razy
    * z użyicem nazwanego semafora
    * 
    * Argumenty: 3 (lub 4)
    * argv[1] - nazwa pliku
    * argv[2] - ile razy ma zostać dodane 1
    * argv[3] - nazwa semafora (opcjonalny - niepodanie spowoduje nieużycie semafora)
    */

    if(argc < 3) {
        printf("critical_section: za mało argumentów\n");
        exit_all();
    }
    srand(time(NULL));
    pid_t pid = getpid();

    // nazwanie argumentów dla czytelności
    char* filename = argv[1];
    int iteration_n = atoi(argv[2]);
    if(argc > 3) {
        semaphore = safe_sem_open(argv[3]);
    }

    // sygnały i atexit
    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("critical_section: błąd ustawiania obsługi sygnału: ");
        exit_all();
    }
    if(signal(SIGUSR1, signal_handler) == SIG_ERR) {
        perror("critical_section: błąd ustawiania obsługi sygnału: ");
        exit_all();
    }
    if(atexit(exit_handler) != 0) {
        perror("critical_section: błąd ustawiania funkcji wyjścia: ");
        exit_all();
    }

    // pętla z sekcją krytyczną
    char buffer[BUFFER_SIZE];
    for(int n = 0; n < iteration_n; n++) {
        sleep(1); // czekanie 1 sek przed sekcją krtyczną

        // przejmowanie semafora
        if(semaphore != NULL) {
            safe_sem_wait(semaphore);
            printf("\t[wewnątrz] wartość semafora: %d\n", safe_sem_getvalue(semaphore));
        }

        // otwarcie pliku i odczytanie liczby
        file = open(filename, O_RDWR, 0644);
        if(file == -1) {
            perror("\tbłąd otwierania pliku: ");
            exit_all();
        }
        int n_read = read(file, buffer, BUFFER_SIZE-1);
        buffer[n_read] = '\0';

        printf("\t[pid: %d] odczytano %s\n", pid, buffer);
        sleep(rand() % 1); // losowe czekanie 0-1 sekund

        // konwersja liczby na tekst liczby i zwiększenie o 1
        int write_size = snprintf(buffer, BUFFER_SIZE, "%d", atoi(buffer) + 1);
        if(write_size < 0) {
            printf("\tbłąd konwertowania liczby na tekst liczby\n");
            exit_all();
        }
        // zamykanie pliku tylko do odczytu
        if(close(file) == -1) {
            perror("\tbłąd zamykania pliku: ");
            exit_all();
        }
        file = -1;

        // ponowne otwieranie tylko do zapisu i czyszczenie pliku
        file = open(filename, O_WRONLY | O_TRUNC, 0644);
        if(file == -1) {
            perror("\tbłąd ponownego otwierania pliku: ");
            exit_all();
        }
        if(write(file, buffer, write_size) != write_size) {
            perror("\tbłąd zapisu danych: ");
            exit_all();
        }
        if(close(file) == -1) {
            perror("\tbłąd zamykania pliku: ");
            exit_all();
        }
        file = -1;

        // koniec sekcji krytycznej - oddawanie semafora
        if(semaphore != NULL) {
            safe_sem_release(semaphore);
            printf("\t[po]wartość semafora: %d\n", safe_sem_getvalue(semaphore));
        }
    }
    return 0;
}

void signal_handler(int signum) {
    exit(EXIT_FAILURE);
}

void exit_handler() {
    if(file != -1) {
        if(close(file) == -1) {
            perror("błąd zamykania pliku: ");
        }
    }

    if(semaphore != NULL) {
        safe_sem_close(semaphore);
    }
}
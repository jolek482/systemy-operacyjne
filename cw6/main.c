/*

Autor: Daniel Florek

Zadanie 6
wzajemne wyklucznanie - semafory

program tworzy kilka procesów potomnych które wykonują sekcję krytyczną
między procesami wykorzystana jest sychronizacja semaforami

*/

#define _POSIX_C_SOURCE 200112L

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <string.h>
#include "lib.h"

#define N_PROC 5

char* semaphore_name = NULL;

void signal_handler(int signum);
void exit_handler();

int main(int argc, char* argv[]) {
    /*
    * Tworzy kilka procesów potomnych uruchamiających program podany w pierwszm argumencie
    * 
    * Argumenty: 5 (lub 6)
    * argv[1] - nazwa programu do inicjowania procesów potomnych
    * argv[2] - liczba procesów potomnych
    * argv[3] - liczba iteracji
    * argv[4] - nazwa pliku wyjściowego
    * argv[5] - nazwa semafora (opcjonalny - niepodanie spowoduje nieużycie semafora)
    */

    if(argc < 5) {
        printf("main: za mało argumentów\n");
        exit(EXIT_FAILURE);
    }

    // przypisanie argumentów do zmiennych (dla czytelności)
    char* program_name = argv[1];
    char path[50];
    if(snprintf(path, 50, "./%s", program_name) < 0) {
        perror("main: błąd tworzenia ścieżki: ");
        exit(EXIT_FAILURE);
    }
    int n_proc = atoi(argv[2]);
    char* iteration_n = argv[3];
    char* filename = argv[4];
    int use_semaphore = (argc > 5);
    if(use_semaphore) {
        semaphore_name = argv[5];
        safe_sem_create(semaphore_name);
    }

    // ustawianie sygnałów
    if(signal(SIGUSR1, SIG_IGN) == SIG_ERR) {
        perror("main: błąd ustawiania obsługi sygnału: ");
        exit(EXIT_FAILURE);
    }
    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("main: błąd ustawiania obsługi sygnału: ");
        exit(EXIT_FAILURE);
    }
    if(atexit(exit_handler) != 0) {
        perror("main: błąd ustawiania funkcji wyjścia: ");
        exit(EXIT_FAILURE);
    }

    // tworzenie u ustawianie pliku
    int file = open(filename, O_CREAT | O_WRONLY | O_TRUNC, 0666);
    if(file == -1) {
        perror("main: błąd tworzenia pliku: ");
        exit(EXIT_FAILURE);
    }
    if(write(file, "0", 1) == -1) {
        perror("main: błąd pierwszego wpisu do pliku: ");
        exit(EXIT_FAILURE);
    }
    if(close(file) == -1) {
        perror("main: błąd zamykania pliku: ");
        exit(EXIT_FAILURE);
    }

    // procesy potomne
    for(int n = 0; n < n_proc; n++) {
        switch(fork()) {
            case -1:
                perror("main: błąd fork: ");
                exit_all();
            case 0:
                if(!use_semaphore) {
                    execlp(path, program_name, filename, iteration_n, (char*)(NULL));
                }
                else {
                    execlp(path, program_name, filename, iteration_n, semaphore_name, (char*)(NULL));
                }
                perror("main: błąd uruchamiania procesu potomnego");
                exit_all();
            case 1:
                continue;
        }
    }

    // oczekiwanie na procesy potomne
    while(wait(NULL) != -1);

    // sprawdzenie poprawnosci wyniku
    file = open(filename, O_RDONLY);
    if(file == -1) {
        perror("main: błąd otwierania pliku: ");
        exit(EXIT_FAILURE);
    }

    char buffer[50];
    int n_read = read(file, buffer, 50-1);
    if(n_read == -1) {
        perror("main: błąd odczytu pliku: ");
        exit(EXIT_FAILURE);
    }
    buffer[n_read] = '\0';

    printf("oczekiwana wartość: %d\n", n_proc * atoi(iteration_n));
    printf("wartość z pliku: %s\n", buffer);

    return 0;
}

void signal_handler(int signum) {
    exit_all();
    while(wait(NULL) != -1);
    
    exit(EXIT_FAILURE);
}

void exit_handler() {
    if(semaphore_name != NULL) {
        safe_sem_delete(semaphore_name);
    }
}
/*

Autor: Daniel Florek

copy_to_file.c
--------
zawiera funkcję przenoszącą dane z jednego pliku do drugiego
w trakcie przenoszenia wypisuje towar na ekranie poprzedzony podanym jako argument tekstem

*/

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include "reader_writer.h"

int copy_to_file(int input_file, int output_file, size_t buffer_size, char * message_prefix) {
    /*
    * Przenosi zawartość z jednego pliku do drugiego
    * W tym zadanniu jednym plikiem jest plik wejściowy albo wyjściowy
    * drugim natomiast nienazwany potok
    *
    * Argumenty
    *   int     input_file      - deskryptor pliku wejściowego (wymaga uprawnień do odczytu)
    *   int     output_file     - deskryptor pliku wyjściowego (wymaga uprawnień do zapisu)
    *   size_t  buffer_size     - jaki rozmiar powinien mieć bufor
    *   char*   message_prefix  - tekst doklejany przed wypisaniem przenoszonego towaru na ekran 
    * 
    * Wartość zwracana
    *  -1   - bład przenoszenia
    *   0   - operacja udana
    */

    char buffer[buffer_size];

    int file_ok = 1; // file_ok == 0 kiedy plik został wyczerpany
    while(file_ok) {
        // odczytywanie z pliku
        ssize_t read_bytes = read(input_file, buffer, buffer_size);
        if(read_bytes != buffer_size) {
            if(read_bytes == -1) {
                printf("błąd w %d\n", getpid());
                perror("błąd odczytywania wejścia: ");
                return -1;
            }
            // else - plik sie skończył i wczytano ile się da
            file_ok = 0; // zapisujemy i potem koniec pętli
        }

        // wypisywanie towaru na ekran
        write(STDOUT_FILENO, message_prefix, strlen(message_prefix));
        if(write(STDOUT_FILENO, buffer, read_bytes) != read_bytes) {
            printf("błąd wypisywania odczytanych danych\n");
        }
        write(STDOUT_FILENO, "\n", 1);

        // zapisywanie do pliku
        if(write(output_file, buffer, read_bytes) != read_bytes) {
            printf("błąd zapisywania danych w pliku\n");
            return -1;
        }

        // sleep z czasem 0-2s żeby zasymulować opóźnienia
        sleep(rand() % 3);
    }

    return 0;
}
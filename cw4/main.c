/*

Autor: Daniel Florek

Zadanie 4

Przy pomocy potoków nienazwanych systemu UNIX zaimplementować problem "Pro-
ducenta i konsumenta". Dla zademonstrowania, że nie doszło do utraty ani zwielokrot-
nienia towaru, niech Producent pobiera"surowiec" (np. porcje bajtów) z pliku tekstowego
i wstawia go jako towar do potoku, a Konsument niech umieszcza pobrany z potoku
towar w innym pliku tekstowym (porcje danych Producenta i Konsumenta nie muszą być
równe). Po zakończeniu działania programów (wyczerpaniu zasobów "surowca”) oba pliki
tekstowe powinny być identyczne (można to sprawdzić poleceniem diff -s , które najlepiej
umieścić w pliku Makefile po poleceniu uruchomienia programu). Oba procesy niech
drukują odpowiednie komunikaty na ekranie, w tym towar, który przesyłają. Do zasymu-
lowania różnych szybkości działania programów użyć funkcji sleep, np. z losowym czasem
usypiania. Do czytania/pisania danych z/do pliku tekstowego, jak również wypisywania
ich na ekran użyć funkcji read i write.

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <string.h>
#include "reader_writer.h"

int main(int argc, char * argv[]) {
    /*
    * Punkt wejściowy
    * Tworzy potok, uruchamia proces potomny i uruchamia w nim kod konsumenta
    * W procesie macierzystym uruchamiany jest kod producenta
    *
    * Argumenty[3]
    * argv[1] - nazwa pliku wejściowego z którego będą wczytywane dane
    * argv[2] - nazwa pliku wyjściowego (jeśli nie istnieje to zostanie utworzony), do którego będą kopiowane dane
    */

    srand(time(NULL));

    if(argc < 3) {
        printf("nie podano nazw plikw wejściowych i wyjściowych\n");
        exit(EXIT_FAILURE);
    }

    int pipedes[2];
    if(pipe(pipedes) == -1) {
        perror("błąd tworzenia potoku: ");
        exit(EXIT_FAILURE);
    }

    switch(fork()) {
        case -1:
            perror("błąd fork: ");
            exit(EXIT_FAILURE);
        case 0:
            /* proces potomny */
            reader(argv[2], pipedes[0]);
            break;
        default:
            /* proces macierzysty */
            writer(argv[1], pipedes[1]);
    }

    return 0;
}
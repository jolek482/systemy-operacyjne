/*

Autor: Daniel Florek

shmlib.c
--------
biblioteka funkcji do obsługi pamięci współdzielonej (z obsługą błędów)

*/

#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include "semlib.h"
#include "shmlib.h"

void safe_shm_create(char* shm_name, int size) {
    int shared_memory = shm_open(shm_name, O_CREAT | O_EXCL | O_RDWR, 0644);
    if(shared_memory == -1) {
        perror("błąd tworzenia pamięci współdzielonej: ");
        exit(EXIT_FAILURE);
    }

    int shm_size = sizeof(SegmentPD);
    if(ftruncate(shared_memory, shm_size) == -1) {
        perror("błąd ustawiania rozmiaru pamięci współdzielonej: ");
        exit(EXIT_FAILURE);
    }

    printf("pamięć współdzielona utworzona [deskryptor: %d, rozmiar: %dB]\n", shared_memory, shm_size);
    safe_shm_close(shared_memory);
}

int safe_shm_open(char* name) {
    int shared_memory = shm_open(name, O_RDWR, 0644);
    if(shared_memory == -1) {
        perror("błąd otwierania pamięci współdzielonej: ");
        exit(EXIT_FAILURE);
    }
    return shared_memory;
}

void safe_shm_close(int shared_memory) {
    if(close(shared_memory) == -1) {
        perror("błąd zamykania pamięci współdzielonej: ");
    }
}

void safe_shm_delete(char* name) {
    if(shm_unlink(name) == -1) {
        perror("błąd usuwania pamięci współdzielonej: ");
    }
}

SegmentPD* safe_shm_map(int shm, int prot) {
    void* ptr = mmap(NULL, sizeof(SegmentPD), prot, MAP_SHARED, shm, 0);
    if(ptr == MAP_FAILED) {
        perror("błąd mapowania pamięci współdzielonej: ");
        exit_all();
    }
    return (SegmentPD*)ptr;
}

void safe_shm_unmap(SegmentPD* ptr) {
    if(munmap(ptr, sizeof(SegmentPD)) == -1) {
        perror("błąd odmapowania pamięci współdzielonej: ");
    }
}
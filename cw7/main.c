/*

Autor: Daniel Florek

Zadanie 7
Przy pomocy potoków nazwanych systemu UNIX zaimplementować problem "Producenta i Konsumenta" z ćwiczenia 4.
(a) Utworzyć potok FIFO z poziomu programu, a następnie uruchomić procesy Producenta i Konsumenta w tym samym programie (w procesie macierzystym i potomnym
lub w dwóch potomnych). Potok usuwać w funkcji zarejestrowanej przez atexit.

*/

#define _POSIX_C_SOURCE 200112L
#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <signal.h>
#include <time.h>
#include "shmlib.h"
#include "semlib.h"

#define PATH_LENGTH 50

char* shm_name = NULL; // nazwa pamięci współdzielonej
char* prod_sem = NULL; // nazwa semafora producenta
char* cons_sem = NULL; // nazwa semafora konsumenta

void signal_handler(int signum);
void exit_handler();

int main(int argc, char * argv[]) {
    /*
    * Punkt wejściowy
    * Tworzy fifo i uruchamia proces producenta i konsumenta
    *
    * Argumenty[8]
    * argv[1] - semafora producenta
    * argv[2] - semafora konsumenta
    * argv[3] - nazwa pamięci współdzielonej
    * argv[4] - program producent
    * argv[5] - program konsument
    * argv[6] - nazwa pliku wejściowego z którego będą wczytywane dane
    * argv[7] - nazwa pliku wyjściowego (jeśli nie istnieje to zostanie utworzony), do którego będą kopiowane dane
    */
    srand(time(NULL));

    if(argc < 6) {
        printf("brakujące argumenty\n");
        printf("./main.x [nazwa pamięci współdzielonej] [program producent] [program konsument] [plik wejścia] [plik wyjścia]\n");
        exit(EXIT_FAILURE);
    }

          prod_sem          = argv[1];
          cons_sem          = argv[2];
          shm_name          = argv[3];
    char* producent_prog    = argv[4];
    char* consument_prog    = argv[5];
    char* in_name           = argv[6];
    char* out_name          = argv[7];

    // sygnały i atexit
    if(atexit(exit_handler) != 0) {
        perror("błąd ustawiania atexit: ");
        exit_handler();
        exit(EXIT_FAILURE);
    }
    if(signal(SIGUSR1, SIG_IGN) == SIG_ERR) {
        // własny sygnał do zakończenia procesów potomnych jest ignorowany
        perror("błąd ustawiania obsługi sygnałów: ");
        exit(EXIT_FAILURE);
    }
    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("błąd ustawiania obsługi sygnałów: ");
        exit(EXIT_FAILURE);
    }

    // inicjalizacja pamięci współdzielonej i semaforów
    safe_shm_create(shm_name, sizeof(SegmentPD));
    // semafor producent = liczba elementów w buforze
    safe_sem_create(prod_sem, NBUF);
    // semafor konsument = liczba elementów w buforze
    safe_sem_create(cons_sem, 0);

    // proces 1 - producent
    char path1[PATH_LENGTH];
    int result = snprintf(path1, PATH_LENGTH, "%s%s", "./", producent_prog);
    if(result < 0 || result > PATH_LENGTH) {
        printf("błąd tworzenia ścieżki programu producenta");
        exit(EXIT_FAILURE);
    }

    switch(fork()) {
        case -1:
            perror("błąd fork: ");
            exit(EXIT_FAILURE);
        case 0:
            execlp(path1, producent_prog, prod_sem, cons_sem, shm_name, in_name, (char*)NULL);
            perror("błąd exec: ");
            exit(EXIT_FAILURE);
    }

    
    // proces 2 - konsument
    char path2[PATH_LENGTH];
    result = snprintf(path2, PATH_LENGTH, "%s%s", "./", consument_prog);
    if(result < 0 || result > PATH_LENGTH) {
        printf("błąd tworzenia ścieżki programu konsumenta");
        exit(EXIT_FAILURE);
    }

    switch(fork()) {
        case -1:
            perror("błąd fork: ");
            exit(EXIT_FAILURE);
        case 0:
            execlp(path2, consument_prog, prod_sem, cons_sem, shm_name, out_name, (char*)NULL);
            perror("błąd exec: ");
            exit(EXIT_FAILURE);
    }

    while(wait(NULL) != -1);    // oczekiwanie na wszystkie procesy potomnes

    return 0;
}

void signal_handler(int signum) {
    printf("otrzymano sygnał SIGINT\n");

    exit_all();                 // zatrzymanie sygnałów potomnych poprzez wysłanie sygnałów
    while(wait(NULL) != -1);    // oczekiwanie na wszystkie procesy potomne

    exit(EXIT_SUCCESS);   
}

void exit_handler() {
    // usuwanie pamięci współdzielonej
    if(shm_name != NULL) {
        safe_shm_delete(shm_name);
    }

    // usuwanie semaforów
    if(prod_sem != NULL)
        safe_sem_delete(prod_sem);

    if(cons_sem != NULL)
        safe_sem_delete(cons_sem);
}
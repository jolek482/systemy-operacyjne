/*

Ćwiczenie 2: Uruchamianie Programów: exec

Zmodyfikować program z ćwiczenia 1b, tak aby komunikaty procesów potomnych były
wypisywane przez program uruchamiany przez funkcję execlp. Nazwę programu do uru-
chomienia przekazywać przez argumenty programu procesu macierzystego.

*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

/* tekst doklejany z lewej strony do nazwy programu */
#define PATH_PREFIX "./"

/* liczba dozwolonych znaków w ścieżce */
#define N 100


int main(int argc, char * argv[]) {
    /*
    *  funckja main wymaga podania jednego argumentu
    *  którym jest nazwa programu do uruchomienia w procesie potomnym
    *  argv[0] - nazwa naszego programu
    *  argv[1] - nazwa pliku podana przez użytkownika
    */
    if(argc < 2) {
        printf("BŁĄD: program wymaga argumentu z programem do wywołania\n");
        exit(EXIT_FAILURE);
    }

    char pathName[N];
    int result = snprintf(pathName, N, "%s%s", PATH_PREFIX, argv[1]); /* skleja nazwę programu w ścieżkę uruchamiania*/
    if (result < 0) {
        perror("BŁĄD TWORZENIA ŚCIEŻKI: ");
        exit(EXIT_FAILURE);
    }

    /* wypisujemy dane o procesie macierzystym */
    printf("uid\tgid\tpid\tppid\tpgid\n\n");
    pid_t pid = getpid(); /* będzie potrzebny do pobrania pgid */
    printf("%d\t%d\t%d\t%d\t%d\n\n", getuid(), getgid(), pid, getppid(), getpgid(pid)); 
    printf("----- procesy potomne -----\n\n");

    int n;
    for(n = 0; n < 3; n++) {
        switch(fork()) {
            case -1:
                perror("BŁĄD FORKOWANIA: ");
                exit(EXIT_FAILURE);
                break;
            case 0:
                /*
                *  proces potomy
                *  uruchamia program podany w pierwszym argumencie wykonywania poprzez execlp
                */
                execlp(pathName, argv[1], (char *)NULL);
                perror("BŁĄD EXECLP: ");
                exit(EXIT_FAILURE);
                break;
            default:
                /*
                *  proces macierzysty
                *  przechodzimy po prostu do następnej iteracji pętli
                */
                continue;
        }
    }

    /* oczekiwanie na procesy potomne */
    for(n = 0; n < 3; n++) {
        if(wait(NULL) == -1) {
            perror("BŁĄD WAIT: ");
        }
    }

    return 0;
}
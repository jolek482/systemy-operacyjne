/*

    Program wypisujący dane o procesie
    Służy jako przykład dla zadania
    Dowolny inny program może zostać przekazany w argumenicie uruchamiania dla main.c

*/

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
    pid_t pid = getpid(); /* będzie potrzebny do pobrania pgid */
    pid_t pgid = getpgid(pid);
    if(pgid == -1) {
        perror("BŁĄD PGID: ");
        exit(EXIT_FAILURE);
    }
    printf("%d\t%d\t%d\t%d\t%d\n\n", getuid(), getgid(), pid, getppid(), pgid); 
    return 0;
}
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

int main() {
    //pid_t pid = getpid(); // będzie potrzebny do pobrania pgid
    printf("uid\tgid\tpid\tppid\tpgid\n\n");
    printf("%d\t%d\t%d\t%d\t%d\n\n", getuid(), getgid(), getpid(), getppid(), getpgid(0)); 
    return 0;
}

#pragma once

void signal_handler(int sig);
int copy_to_file(int input_file, int output_file, size_t buffer_size, char * message_prefix);
/*

Autor: Daniel Florek

writer.c
--------
kod producenta danych
odczytuje dane z podanego pliku (w argumencie funkcji main)
i przenosi je do potoku

*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include "copy_to_file.h"

void exit_handler();

int pipedes;
int input_file;

int main(int argc, char* argv[]) {
    /*
    * Argumenty[3]
    * argv[1] - nazwa potoku
    * argv[2] - plik wejściowy
    */
    if(argc < 3) {
        printf("nie podano nazwy potoku i pliku wejsciowego w argumentach\n");
        exit(EXIT_FAILURE);
    }

    input_file = open(argv[2], O_RDONLY);
    if(input_file == -1) {
        perror("błąd otwierania pliku wejściowego");
        exit(EXIT_FAILURE);
    }

    pipedes = open(argv[1], O_WRONLY);
    if(input_file == -1) {
        perror("błąd otwierania potoku");
        if(close(input_file) == -1) {
            perror("błąd zamykania pliku wejściowego: ");
        }
        exit(EXIT_FAILURE);
    }

    if(atexit(exit_handler) != 0) {
        perror("błąd ustawiania atexit: ");
        exit_handler();
        exit(EXIT_FAILURE);
    }
    if(signal(SIGINT, signal_handler) == SIG_ERR) {
        perror("błąd ustawiania obsługi sygnałów: ");
        exit(EXIT_FAILURE);
    }

    // przenoszenie danych z pliku do potoku
    int result = copy_to_file(input_file, pipedes, 32, "> ");
    printf("zapisywanie zakończone\n");

    // zamykanie z odpowiednim kodem
    switch(result) {
        case -1:
            exit(EXIT_FAILURE);
        default:
            exit(EXIT_SUCCESS);
    }
}

void exit_handler() {
    if(close(pipedes) == -1) {
        perror("błąd zamykania potoku: ");
    }

    if(close(input_file) == -1) {
        perror("błąd zamykania pliku: ");
    }
}